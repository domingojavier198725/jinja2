""" En base a la aplicación desarrollada en la Sustitución de CLI por Web,
 debe asegurarse de utilizar el motor de plantilla Jinja2."""

"""En esta aplicación me aseguré de utilizar el motor de busqueda Jinja2. La afunción render_template se
  utiliza Jinja2 https://plataforma.josedomingo.org/pledin/cursos/flask/curso/u15/"""

from flask import Flask, render_template, request
import redis


servidor = redis.Redis("localhost")

app = Flask(__name__)


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/agregar', methods=['GET', 'POST'])
def add():
    if request.method == 'POST':
        palabra = request.form["palabra"]
        definicion = request.form["definicion"]
        servidor.hset('diccionario', 'palabra', palabra)
        servidor.hset('diccionario', 'definicion', definicion)
        return render_template("success.html")
    return render_template("add.html")


@app.route('/editar', methods=['GET', 'POST'])
def edit():
    if request.method == 'POST':
        palabra = request.form["palabra"]
        servidor.hset('diccionario', 'palabra', palabra)
        return render_template("success.html")
    return render_template("edit.html")


@app.route('/eliminar', methods=['GET', 'POST'])
def delete():
    if request.method == 'POST':
        #palabra = request.form["palabra"]
        servidor.hdel('diccionario', 'palabra')
        servidor.hdel('diccionario', 'definicion')
        return render_template("success.html")
    return render_template("delete.html")


@app.route('/ver')
def ver():
    if servidor.hexists('diccionario', 'palabra') and servidor.hexists('diccionario','definicion'):
        all = str(servidor.hgetall('diccionario'))
        return f"Los datos son: \n{all}"
    else:
        return render_template("sindata.html")


@app.route('/buscar')
def buscar():
    if servidor.hexists('diccionario', 'palabra'):
        data = str(servidor.hget('diccionario','palabra'))
        data1 = str(servidor.hget('diccionario','definicion'))
        return f"Los datos son: \nPalabra: {data} Definción: {data1}"
    else:
        return render_template("sindata.html")


if __name__ == "__main__":
    app.run(host='localhost', port=80)
